# Požadavek na poskytnutí doplňujících podkladů od sdružení LEX


Vážená paní/Vážený pane,

jako dobrovolník v rámci Pirátské Strany se zabývám problematikou zprávy ECHA o
omezení olova ve střelách.[^fn1] Mým úkolem bylo posoudit stanoviska LEX
týkající se dané směrnice.[^fn2] Rád bych Vás touto cestou požádal o odpověď na
několik otázek:

1) Zběžně jsem prolistoval zprávu ECHA ve verzi 1.4.[^fn3] Vzhledem k datům
předpokládám, že toto je verze na kterou LEX reaguje ve svých vlastních
analýzách a studiích.  Poslední veřejně dostupné stanovisko LEX je zpráva
převzatá z partnerské federace ESSF datovaná k 27. 7. 2021. Aktuální verze
reportu ECHA je ze 24. března 2021. Má LEX vypracovaná věcná stanoviska ke
zprávě ECHA ve verzi 2.0? Daná zpráva se jeví argumentačně podstatně
vyzrálejší. Zároveň se domnívám, že došlo ke zohlednění potřeb střelecké
komunity, ale nevím v jak velkém rozsahu.

2) Které nedostatky na které poukazovalo sdružení LEX byly v nové zprávě
adresovány a v jakém rozsahu? 

3) Které nedostatky na které poukazovalo sdružení LEX nebyly v nové verzi
zprávy adresovány?

4) Mohl by LEX poskytnout návrh jakým způsobem by mělo docházet k předcházení
otravám ptactva olověnými broky? Jak silný zásah by byl například zákaz
používání olova pro lov brokovnicí?

5) Mohl by LEX poskytnout návrh jakým způsobem by mělo docházet ke snížení
zátěže olovem pro mrchožrouty? Jakým způsobem by například mělo docházet ke
kontrole, že vyříznuté kontaminované maso okolo střely je řádně zajištěno a
nezůstává v prostředí?

6) Mohl by LEX do budoucna ve svých analýzách přizvat odborníky z dalších
dotčených oborů (ochrana životního prostředí, toxikologie) a tím zvýšit
věrohodnost a ucelenost svých zpráv? V případě vyhotovení zpráv čistě sdružením LEX dochází
ke zjevnému konfliktu zájmů. Tento konflikt zájmů by participace ostatních
dotčených stran citelně snižovala a zvyšovala tím věrohodnost objektivity zpráv
poskytovaných sdružením LEX.

Pevně věřím, že cílem sdružení LEX je ochrana práv střelecké komunity. Součástí
střelecké komunity jsou i myslivci, kteří mají přirozeně zájem na udržení a
rozvoji přírodního bohatství. Jelikož je ochrana zdraví cílem nás všech, je i v
zájmu LEX snaha o snížení, až eliminaci emisí olova plynoucí ze střelecké
činnosti. Tato snaha nemusí dle mého skromného názoru nutně znamenat to samé,
jako zákaz olova.

S pozdravem,

Jan Zelenka

[^fn1]: https://echa.europa.eu/cs/registry-of-restriction-intentions/-/dislist/details/0b0236e1840159e6
[^fn2]: https://gunlex.cz/clanky/hlavni-clanky/3265-analyza-lex-z-s-ke-zprave-echa-doporuceni-k-uplnemu-zakazu-oloveneho-streliva
https://gunlex.cz/clanky/hlavni-clanky/3602-vlivy-oloveneho-a-bezolovnateho-streliva-na-zivotni-prostredi
https://gunlex.cz/clanky/hlavni-clanky/3598-vliv-zveriny-ulovene-olovenym-strelivem-na-lidske-zdravi
https://gunlex.cz/zbrane-a-legislativa/legislativa/3948-zprava-echa-o-temer-uplnem-zakazu-olova-znovu-ze-spatneho-konce
https://gunlex.cz/clanky/hlavni-clanky/4003-test-ocelovych-a-olovenych-broku-v-ramci-pripominek-pro-echa
https://gunlex.cz/clanky/hlavni-clanky/3594-komparacni-test-oloveneho-a-neoloveneho-streliva
[^fn3]: https://echa.europa.eu/cs/-/echa-identifies-risks-to-terrestrial-environment-from-lead-ammunition
