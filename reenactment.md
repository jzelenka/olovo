# Požadavek na poskytnutí doplňujících podkladů od sdružení LEX - sběratelství, historické rekonstrukce


Vážená paní/Vážený pane,
jako dobrovolník v rámci Pirátské Strany zjišťuji potřeby komunit
střelců-sběratelů a střelců zabývajících se historickými rekonstrukcemi
(reenactment). Obracím se na Vás jakožto sdružení s kontakty na dané komunity a
celkovým dobrým vhledem do problematiky s následujícími dotazy:

1) Jsou v českém zákonu ustanovení komplikující život sběratelům? Která?

2) Jsou v českém zákonu ustanovení komplikující život střelcům zabývajícím se
historickými rekonstrukcemi? Která?

3) Pokud ano, můžete navrhnout právní úpravu která by dané problémy řešila s
nulovým, či marginálním dopadem na celkovou bezpečnost státu? Narážím zde
například na nebezpečí snadné reaktivace znehodnocených zbraní, formování
paramilitárních skupin a další jevy úzce související se státní bezpečností.

4) Šel by český zákon v těchto oblastech nějakým způsobem zjednodušit/zkrátit
pro zlepšení jeho čitelnosti a zjednodušení pravidel, bez dopadu na jeho
funkčnost?

5) Z doslechu jsem slyšel, že účastník rekonstrukce historických bitev z II.
světové války byl neprávem sledován a konfrontován PČR pro podezření z
extremismu (účast ve skupině dopouštějící se násilí proti skupině obyvatel či
proti jednotlivci a podněcování k nenávisti vůči skupině osob nebo k omezování
jejich práv a svobod). Je daný jev častý? Můžete poskytnout své odborné
stanovisko, jak by šlo takovým incidentům předcházet bez toho, aby daná úprava
stavu výrazně usnadnila život extremistickým uskupením?

S pozdravem,
Jan Zelenka
