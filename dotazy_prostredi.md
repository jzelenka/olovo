# Zdroje znečištění životního prostředí olovem - Česká Republika

Vážený pane Blažej,

obracím se na Vás s několika otázkami jakožto na kontaktní osobu z týmu
"Životní prostředí." Jsem dobrovolník pirátské strany z podtýmu "Zbraně a
bezpečnostní materiál." V podtýmu se zabýváme aktuálně doporučením ECHA ohledně
olova ve střelách.[^fn1] Rádi bychom vypracovali stanovisko za které by se
Pirátská Strana nemusela stydět a naopak by ho mohla hrdě hájit.

Pokud by došlo k výraznému omezení dostupnosti olověných střel, došlo by k
citelnému zásahu do života rekreačních střelců, sportovních střelců, myslivců a
sběratelů. V týmu jsme si vědomi negativních dopadů olova na vývoj a kognitivní
schopnosti člověka a jeho dopadů na životní prostředí. Na druhou stranu z
materiálů nám dostupných se nám jeví střelecká komunita jako marginální zdroj
kontaminace olovem. Jelikož je naše specializace jinde než v ochraně životního
prostředí, rádi bychom Vás požádali o zodpovězení následujících otázek:

1) Dle review na které jsem narazil jsou majoritními znečišťovateli v globálním
měřítku olověné doly a slévárny.[^fn2] Odpovídá tato situace i zdrojům
znečištění v České republice? Je známo jak velký podíl znečištění v ČR má na
svědomí střelecká komunita, resp. jak velký podíl mají jiné zdroje? Pokud ano,
mohu poprosit o odkaz na studii a stanovisko Vašeho týmu?

2) Z metastudie sdružení LEX[^fn3] vyplývá, že hlavními reálnými problémy ke
kterým dochází je kontaminace mrchožroutů olovem a pozření olověných broků
ptactvem. Je toto realita? O jak velký problém se v českých podmínkách reálně
jedná? Existují další problémy související s životním prostředím o kterých se
metastudie nezmiňuje, či je bagatelizuje?

3) Ví se něco o dopadech bismutu a wolframového prášku (velký povrch ->
rychlejší vstřebávání do ekosystému) na životní prostředí? Mám jakožto chemik
pocit, že to nic zdravého asi nebude, ale nejsem si jist jak moc toxický je
jejich dopad. Mohu požádat o stanovisko?

Vzhledem k mému vzdělání je mi zřejmé, že v případě olova, jakožto látky bez
prahové hodnoty co se týče škodlivosti, je jediná správná hodnota v životním
prostředí 0. Na druhou stranu je olovo pro střeleckou komunitu díky svým
unikátním vlastnostem prakticky nenahraditelné. Mým cílem je nalézt možnosti,
jak minimalizovat emise olova pocházející ze střelecké komunity při zachování
svobody tento sport, či koníček provozovat. Daný zásah do práv jednotlivce by
mi přišel krajně neadekvátní zvlášť, pokud by byly reálné dopady na životní 
prostředí pouze marginální, či na prahu měřitelnosti.

S pozdravem,

Jan Zelenka



## Citace
[^fn1]: https://echa.europa.eu/cs/registry-of-restriction-intentions/-/dislist/details/0b0236e1840159e6
[^fn2]: E. Obeng-Gyasi, Reviews on Environmental Health 2019, 34, 25–34. https://www.degruyter.com/document/doi/10.1515/reveh-2018-0037/html
[^fn3]: bod 2.2 https://gunlex.cz/clanky/hlavni-clanky/3265-analyza-lex-z-s-ke-zprave-echa-doporuceni-k-uplnemu-zakazu-oloveneho-streliva
