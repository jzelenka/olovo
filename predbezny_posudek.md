# Předběžný posudek analýz a testů provedených sdružením LEX a zprávách ECHA

Při zběžném projití zprávy ECHA (V1.4)[^fn1] a přidružených článků[^fn2] jsem došel k
závěru, že dané studie a zprávy neposkytují ucelený obraz o problematice.
Dochází zde k nadměrné extrapolaci negativních dopadů užívání olova na životní
prostředí a na lidské zdraví. Zároveň jsou zde naprosto bagatelizovány a
ignorovány komplikace pro střeleckou komunitu. Daleko vyváženěji a lépe v tomto
ohledu působí analýzy a testy LEX kde nedochází ke "kreativní interpretaci
dat." Na druhé straně je v analýzách[^fn3],[^fn4],[^fn5] a testech[^fn6],[^fn7] LEX
logicky věnován velký prostor nedostatkům zprávy ECHA (V1.4) a pouze malý prostor
pozitivním dopadům implementace byť jen částí zprávy ECHA (V1.4). Analýzy lex z tohoto
pohledu tedy nejsou vyvážené.

Doporučení ECHA (V1.4) je v hrubém rozporu s principem proporcionality a je silně
tendenční a bezohledné vůči celé střelecké komunitě. Ve zprávách LEX dochází k
mírné bagatelizaci důsledků kontaminace olovem a zároveň nenavrhují žádné
řešení problému otrav ptactva který přiznávají, že existuje.[^fn3] Bylo by
zajímavé provést panelovou diskuzi se zástupci myslivecké komunity a ochránců
přírody ve snaze najít optimální řešení jak předcházet otravám drobného
ptactva.

## Předpoklady
Neověřoval jsem, jak dobře šli ke zdroji ve zprávách LEX. U zprávy ECHA (V1.4) toto
ověření provedlo LEX. Na základě toho, že LEX přiznává i citace namířené proti
jeho zájmu předpokládám, že sdružení odvedlo v tomto směru dobrou práci.

## TODO

 * projet ECHA 2.0
 * Dodělat citace
 * Ověřit na základě dat z citací správnost mého stanoviska.


## Pociťované nedostatky zprávy ECHA ve verzi 1.4

 * Špatná argumentace pro plošný zákaz olova na venkovních střelnicích
(kontaminaci životního prostředí lze předejí).
 * Absence diskuze možnosti odražení střel a z toho plynoucí zvýšení ohrožení střelců.
 * Ignorace toxicity bezolovnatých střel.
 * Nadhodnocení dopadů používání olova ve střelách.
 * Porušení principu proporcionality jak je definován v právu EU.


## Pociťované nedostatky zpráv a testů LEX

 * U testů (neplatí pro analýzy) nejspíše nebyla provedena řádná literární
rešerše která by uvedla testy do souvislostí s již existující literaturou.
 * V případě nehodících se výstupů velmi často vídaná formulace "bude potřeba dalších studií...."
 * Částečná bagatelizace rizika pro děti a vyvíjející se nervový systém.
 * Absence návrhu řešení pro předcházení otravám ptactva.
 * Nedeklarovaný konflikt zájmů (uznávám, že LEX asi počítá s tím, že tento konflikt je zjevný).


## Plné osobní stanovisko

Je prokázáno, že olovo a další těžké kovy mají škodlivé účinky mimo jiné na
vývoj mozku, potažmo centrálního nervového systému. Zároveň u olova nejspíše
neexistuje bezpečná expozice, u které by nedocházelo k negativním dopadům. V
důsledku těchto zjištění dochází k postupné regulaci a eliminaci olova z lidské
činnosti a životního prostředí.

Problémem při odstraňování olova je, že má unikátní fyzikálně-chemické
vlastnosti a dost často je tedy nejlevnějším, nejdostupnějším a nejlepším
materiálem při různých činnostech - například rybaření a sportovní střelbě.
Cílem doporučení ECHA je pokročit v úplném odsranění olova z našich životů.
Tato snaha je součástí snahy o ochranu evropské populace a přírody před těžkými
kovy.

Zásadním nedostatkem doporučení uvedeném ve zprávě ECHA (v1.4) je, že by vedla k
nahrazení olova při lovu jinými těžkými kovy s neznámou a v některých případech
nejspíš srovnatelnout toxiciou (wolfram, bismut, ...).[CIT..] V důsledku by
tedy z pohledu celkové toxicity docházelo minimálně částečně k vytloukání klínu
klínem. Zároveň s tím zpráva ECHA (v1.4) tendenčně bagatelizuje a opomíjí negativní
dopady pro střeleckou komunitu. Velmi silné dopady na střeleckou komunitu jsou
v rozporu s principem proporcionality a deklarovanou snahou EU o zachování
kulturní diverzity. Přijmutím doporučení ECHA (v1.4) by došlo k marginálnímu
pozitivnímu dopadu na životní prostředí, s vyjímkou kontaminace drobného ptactva.
V případě zákazu olova pro střeleckou a rybářskou komunitu jsou jeho pozitivní
dopady na lidské zdraví na hraně měřitelnosti.

Dle mého názoru by mělo dojít k diskuzi mezi loveckou komunitou, která je
přímým kontaminantem životního prostředí a ochránci životního prostředí.
Lovecká komunita by ve spolupráci s ochranáři měla vypracovat společné
stanovisko o omezení použití těžkých kovů které by vedlo k omezením negativních
dopadů na životní prostředí, zvěř a ptactvo při zachování principu proporcionality. Vzhledem k
minoritnímu dopadu na lidskou populaci by krom problému otrav
ptactva nemělo dojít k omezení použití olova, jelikož by se jednalo o hrubé
porušení principu proporcionality tak, jak je definován v právu EU. Měla by být
provedena analýza toxicity těžkých kovů nahrazujících olovo a jejich vlivu na
životní prostředí. Z dlouhodobé perspektivy by měla být stimulována snaha
střelecké komunity o nalezení alternativního střeliva neobsahující jakékoliv
toxické těžké kovy, nikoliv pouze olovo. K omezení a případnému zákazu olova a
jiných těžkých kovů ve střelách by mělo dojít ve chvíli, kdy budou dostupné
alternativy zajišťující ctění principu proporcionality.

## O Autorovi

### Konflikt zájmů
Jsem v konfliktu zájmů jakožto držitel zbraní a zbrojního průkazu. Jsem v kontaktu se střeleckou komunitou, výrobci a distributory zbraní a střeliva. Případné omezení používání olova by mne tedy osobně zasáhlo. Tento konflikt zájmů je částečně či plně komenzován mým zájmem o mé osobní zdraví, zájmem o stav a zdraví krajiny jako celku a v neposlední řadě tím, že mám děti, kterým bych rád tuto krajinu předal v neznečištěním stavu.

### Relevance
Mé vysoškolské chemické vzdělání a osobní studium toxikologie mne dovoluje plně chápat možné důslekdy jak akutní, tak chronické otravy olovem a důsledky pronikání olova do životního prostředí.

## Nezatříděné odkazy
DeGruyter: https://www.degruyter.com/document/doi/10.1515/reveh-2018-0037/html - vypadá to, že střelci jsou minoritními znečišťovateli.

Hodnotící US článek: https://extension.okstate.edu/fact-sheets/impacts-of-lead-ammunition-and-sinkers-on-wildlife.html

Olovo Wiki EN: https://en.wikipedia.org/wiki/Lead#Biological_effects

## citace

[^fn1]: https://echa.europa.eu/cs/-/echa-identifies-risks-to-terrestrial-environment-from-lead-ammunition
[^fn2]: N. Kanstrup, V. G. Thomas, Environmental Sciences Europe 2020, 32, 91. (https://doi.org/10.1186/s12302-020-00368-9)
[^fn3]: https://gunlex.cz/clanky/hlavni-clanky/3265-analyza-lex-z-s-ke-zprave-echa-doporuceni-k-uplnemu-zakazu-oloveneho-streliva
[^fn4]: https://gunlex.cz/clanky/hlavni-clanky/3602-vlivy-oloveneho-a-bezolovnateho-streliva-na-zivotni-prostredi
[^fn5]: https://gunlex.cz/clanky/hlavni-clanky/3598-vliv-zveriny-ulovene-olovenym-strelivem-na-lidske-zdravi
[^fn6]: https://gunlex.cz/zbrane-a-legislativa/legislativa/3948-zprava-echa-o-temer-uplnem-zakazu-olova-znovu-ze-spatneho-konce
[^fn7]: https://gunlex.cz/clanky/hlavni-clanky/4003-test-ocelovych-a-olovenych-broku-v-ramci-pripominek-pro-echa
[^fn8]: https://gunlex.cz/clanky/hlavni-clanky/3594-komparacni-test-oloveneho-a-neoloveneho-streliva
